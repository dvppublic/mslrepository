# MSLRepository

Repository for MSL project.

    1. Follow the bellow link to install SwiftLint; And add the SwiftLint Run Script
https://iphoneappcode.blogspot.co.uk/2017/03/how-to-integrate-swiftlint-library-into.html
`
if which swiftlint >/dev/null; then
    swiftlint
else
    echo "warning: SwiftLint not installed, download from https://github.com/realm/SwiftLint , More info at: https://iphoneappcode.blogspot.co.uk/2017/03/how-to-integrate-swiftlint-library-into.html"
    exit 1
fi`

    2. Create custom .swiftlint.yml file.
https://stackoverflow.com/questions/43109593/how-do-i-create-swiftlint-yml-file-where-i-need-to-put-it
https://github.com/realm/SwiftLint#configuration - sample configuration of the .swiftlint.yml file.

Initial Setup

0. Show hidden MacOS files.
1. Install SwiftLint
2. Add SwiftLint Run Script to the project's target.
3. Add custom .swiftlint.yml file.
4. Config pre-commit hook.
5. (Optional)  Install GUI XCode Source Extension - SwiftLintForXcode.

Setup for a team member

1. Clone Repo.
2. Execute `git config core.hooksPath hooks` from main project's directory.

